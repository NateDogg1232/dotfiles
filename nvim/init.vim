set encoding=utf-8

" ------------
" ---- Plugins

" Plugin management for nvim
call plug#begin('~/.local/share/nvim/plugged')

" Powerline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" Pywal
Plug 'dylanaraps/wal.vim'
" Tmux config support
Plug 'tmux-plugins/vim-tmux'
" Autocompletion per CoC
Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}
" Packaging info
Plug 'meain/vim-package-info', { 'do': 'npm install' }

call plug#end()


" -----------------------
" --- Basic configuration
" Show linenumbers
set relativenumber
set number
" Extend the command line height
set cmdheight=2
" Don't give |ins-completion-menu| messages.
set shortmess+=c
" Show signcolumns
set signcolumn=yes
" Enable mouse
set mouse=a
" Enable incremental search
set incsearch
" Enable showing matching brackets
set showmatch
" Change tabs
set softtabstop=4
" Smaller update for CursorHold & CursorHoldI
set updatetime=300

" ----------------
" ---- Colorscheme

" Set the colorscheme to use wal
colorscheme wal

" -------------------
" ---- Airline config

" Set it to not use powerline symbols
let g:airline_symbols_ascii = 1
" Set the theme to simple
let g:airline_theme = "simple"

" ---------------
" ---- CoC config
inoremap <silent><expr> <TAB>
	\ pumvisible() ? "\<C-n>" :
	\ <SID>check_back_space() ? "\<TAB>" :
	\ coc#refresh()
inoremap <expr><S-TAB> pumvisible

function! s:check_back_space() abort
	let col = col('.') - 1
	return !col || getline('.')[col - 1] =~# '\s'
endfunction

" Use <c-space> for trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> for confirm completion, `<C-g>u` means break undo chain at current
" position
" CoC only does snippet and additional edit on confirm
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[c` and `]c` for navigating diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)

"Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K for showing documentation in the preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
	if &filetype == 'vim'
		execute 'h '.expand('<cword')
	else
		call CocAction('doHover')
	endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
vmap <leader>f <Plug>(coc-format-selected)
nmap <leader>f <Plug>(coc-format-selected)

augroup mygroup
	autocmd!
	" Set up formatexpr for specified filetypes
	autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
	" Update signature help on jump placeholder
	autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap to do codeAction on selected region, ex: `<leader>aap` for current
" paragraph
vmap <leader>a <Plug>(coc-codeaction-selected)
nmap <leader>a <Plug>(coc-codeaction-selected)

" Remap to do codeAction on current line
nmap <leader>ac <Plug>(coc-codeaction)
nmap <leader>qf <Plug>(coc-fix-current)

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use :Fold to fold the current buffer
command! -nargs=? Fold :call CocAction('fold',<f-args>)
