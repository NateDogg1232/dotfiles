#!/usr/bin/fish

set bg (ls -I config.template ~/.config/fonoj/ | rofi -dmenu -no-custom -p fono) && echo $bg > ~/.config/bg.cfg && wal.fish
