#!/usr/bin/fish

set bg (cat ~/.config/bg.cfg)
set bg_img ~/.config/fonoj/$bg/img
set bg_file ~/.config/fonoj/$bg/config

function get_config_thing
	cat $bg_file | awk " /$argv[1]/ {print \$3}"
end

if not test -f $bg_file
	echo "bg config file not found"
	exit 1
end

rm ~/.cache/wal/schemes/*

wal -i $bg_img (get_config_thing light | awk " /true/ {print \"-l\"}")\
	--backend (get_config_thing backend)

echo Running post-wal scripts "(Ran in parallel)"
echo Logs in ~/.logs/wal.d/

# Do all the post-run stuff
for script in ~/.config/scripts/wal.d/*.fish
	set -l scfile (string split "/" "$script")[-1]
	echo Running $scfile
	date  | tee ~/.logs/wal.d/$scfile.log >> ~/.logs/wal.d/$scfile-err.log
	$script >> ~/.logs/wal.d/$scfile.log 2>> ~/.logs/wal.d/$scfile-err.log &
end
