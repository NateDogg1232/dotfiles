#!/usr/bin/fish

pulseaudio --kill
pulseaudio --check && echo "Pulseaudio already running" > ~/.logs/pulseaudiostart.log
pulseaudio --start -D --fail 2> ~/.logs/painfo.log
sleep 1
pactl info > ~/.logs/pulseaudioinfo.log
mpd &
