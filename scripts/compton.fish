#!/usr/bin/fish

date | tee ~/.logs/compton.log -a
echo "Compton startup" | tee ~/.logs/compton.log -a

compton -b\
	--backend glx\
	--blur-background --blur-method kawase --blur-strength 10 --blur-background-fixed -f\
	--vsync opengl-swc --glx-use-gpushader4 --glx-copy-from-front --glx-no-stencil --glx-no-rebind-pixmap\
	| tee ~/.logs/compton.log -a --output-error=warn


