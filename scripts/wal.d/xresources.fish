#!/usr/bin/fish

cat ~/.config/xres/.Xresources ~/.cache/wal/colors.Xresources > ~/.Xresources
xrdb ~/.Xresources


#Check if this computer is a desktop
if not test -f ~/.desktop
	exit
end


# Set the background for URxvt
set -l bg "[65]"(xrdb -query | grep color0: -m 1 | awk "{print \$2}")
echo "URxvt*background:	$bg" >>  ~/.Xresources
echo "URxvt*borderColor:	$bg" >> ~/.Xresources
xrdb -merge ~/.Xresources


