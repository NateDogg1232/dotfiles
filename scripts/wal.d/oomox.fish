#!/usr/bin/fish

# Make sure we don't regenerate the theme even though it's the same exact background
if cmp ~/.config/bg.cfg ~/.config/oomox/bg.cmp
	echo "Same background. Gonna ignore"
	exit
end


# Icons
/opt/oomox/plugins/icons_numix/change_color.sh -o wal-icons ~/.cache/wal/colors-oomox
# GTK Theme
oomox-materia-cli -o wal-theme ~/.cache/wal/colors-oomox

# Save the current bg for later
cp ~/.config/bg.cfg ~/.config/oomox/bg.cmp
