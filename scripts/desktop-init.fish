#!/usr/bin/fish

set -l logfile ~/.logs/desktop-init.log
set -l dolog tee $logfile -a

touch $logfile

date | $dolog
echo "desktop-init.fish" | $dolog

if not test -f ~/.desktop
	echo "not a desktop" | $dolog
	exit
end

echo "a desktop" | $dolog

~/.config/scripts/compton.fish

