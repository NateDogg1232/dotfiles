#!/usr/bin/fish

terminit.fish
start-pulseaudio-x11
nm-applet &
pa-applet &
xrdb ~/.Xresources
desktop-init.fish
wal.fish
flameshot &
