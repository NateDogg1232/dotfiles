#!/usr/bin/fish

mpc $argv[1]
exit

# All of this is just in case I switch over to a different music player

switch "$argv[1]"
	case next
		mpc next
	case prev
		mpc prev
	case pause
		mpc pause
	case play
		mpc play
	case toggle
		mpc toggle
	case stop
		mpc stop
end
