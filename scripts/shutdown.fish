#!/usr/bin/fish

set commands "sudo shutdown now" "sudo systemctl suspend" "sudo systemctl hibernate" "reboot" "i3 exit" "exit";
#set commands "echo shutdown" "echo sleep" "echo hibernate" "echo reboot" "echo logout" "echo exit";

set result (echo "1 Halti
2 Halteti
3 Letargiigi
4 Restartigi
5 Elsaluti
6 Nuligi" | rofi -dmenu -p "Opcioj de Haltado" -l 6 -no-custom | awk "{print \$1}")

eval $commands[$result]
