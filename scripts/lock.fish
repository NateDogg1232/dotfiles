#!/usr/bin/fish


i3lock	-B=50 --bar-indicator --bar-orientation=horizontal\
	--bar-step=100\
	--bar-periodic-step=2\
	--bar-color (xrdb -query | grep color1: -m 1 | awk "{print \$2}")ff\
	--indicator -k\
	--keyhlcolor (xrdb -query  | grep color7: -m 1 | awk "{print \$2}")ff\
	--timecolor (xrdb -query | grep color7: -m 1 | awk "{print \$2}")ff\
	--timestr "%_I:%M:%S %p"\
	--datecolor (xrdb -query | grep color8: -m 1 | awk "{print \$2}")ff\
