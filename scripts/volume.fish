#!/usr/bin/fish

switch (echo $argv[1])
	case up
		pamixer -i 5
	case down
		pamixer -d 5
	case mute
		pamixer -m
	case unmute
		pamixer -u
	case toggle
		pamixer -t
	case info
		pamixer --get-volume-human
end
