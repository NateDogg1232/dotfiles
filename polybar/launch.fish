#!/usr/bin/fish

echo "killing polybar"
killall -q polybar
while pgrep polybar > /dev/null
	sleep 1
end
echo "starting polybar"

if test -f ~/.desktop
	echo "is desktop"
	polybar -r mainbar-desktop &
	polybar -r mainbar2 &
else
	echo "is laptop"
	polybar -r mainbar &
end

echo "Polybar launched"
