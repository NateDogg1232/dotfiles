# Nate's dotfiles

Yeah. These are my dotfiles. It's not pretty, I know, but it is what it is.

There's also lukesmithxyz's st fork in my home folder which I use as my terminal which is not included as all its configurations are from a computer-to-computer basis for me and it's close to unmodified for me. It only has the inclusion of wal's include file and that's it, really. I also changed it to work with rofi instead of dmenu.

## How to use:

This whole folder is a clone of my .config folder. So pretty much just clone this as your .config folder and call it a day. There's some config files like the bg.cfg file which I made myself. This is used in the wal.fish script in the scripts folder.

## Prerequesites (or however you spell that)

Honestly, I've been doing this for so long I don't remember, but these are the ones I can remember off the top of my head:

- st
- i3-gaps
- i3-lock-color
- compton (tryone144's fork)
- fish
- rofi
- pywal (and all its prerequisites)
- polybar
- ranger
- w3m
- ncmpcpp (and mpd, of course)
- pulseaudio
- scrot
- vim
- firefox-developer-edition
- nm-applet (for NetworkManager)
- pa-applet (for PulseAudio)
- amixer (for audio adjustment)
- mpc (for control of mpd in i3)
- mpstat (for CPU gauge in polybar)
- undefined-medium font (I use it almost everywhere)
